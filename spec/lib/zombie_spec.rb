require 'spec_helper'
require 'lib/zombie'

#describe 'A Zombie' do
#  it "is named Ash"
#end

describe Zombie do
  zombie = Zombie.new
  it "is named Ash" do
    zombie.name.should == 'Ash'
    #expect(zombie.name).to eq('Ash')
  end

  it "has no brains" do
    zombie.brains.should < 1
  end

  it "loves food" do
    #expect(/foo/).to match("food")
  end

  it ".alived" do
    zombie.alive.should == false
  end

  it ".rotting" do
    zombie.rotting.should == true
  end

  it ".hungry" do
    zombie.hungry.should == true
  end

  it "has much height" do
    zombie.height.should be > 5
  end

end

