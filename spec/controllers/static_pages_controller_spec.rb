require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do

  #render_views

  it "has home page" do
    get :home
    expect(response.status).to eq(200)
    #expect(response).to render_template("home")
    #assert_select "title", "Ruby on Rails Tutorial Sample App"
  end

  it "has help page" do
    get :help
    expect(response.status).to eq(200)
  end

  it "has about page" do
    get :about
    expect(response.status).to eq(200)
  end

  it "has contact page" do
    get :contact
    expect(response.status).to eq(200)
  end

end
