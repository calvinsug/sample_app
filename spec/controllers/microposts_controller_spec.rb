require 'rails_helper'

RSpec.describe MicropostsController, type: :controller do

  fixtures :microposts, :users

  describe "GET #home" do

    it "redirect create when not logged in" do
      post :create, { :microposts => { :content => "Welcome to Rspec" } }
      expect(response).to redirect_to login_url
    end

    it "redirect destroy when not logged in" do
      @micropost = microposts(:orange)
      delete :destroy, :id => @micropost
      expect(response).to redirect_to login_url
    end

    it "redirect destroy when wrong micropost" do
      log_in_as(users(:calvin))
      @micropost = microposts(:ants)
      delete :destroy, :id => @micropost
      expect(response).to redirect_to root_url
      #assert_redirected_to root_url
    end


  end


end
