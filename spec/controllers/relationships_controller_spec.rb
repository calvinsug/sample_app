require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do

  fixtures :relationships

  it "require logged-in user when create" do
    post :create
    expect(response).to redirect_to login_url
  end

  it "require logged-in user when destroy" do
    delete :destroy, id: relationships(:one)
    expect(response).to redirect_to login_url
  end

end
