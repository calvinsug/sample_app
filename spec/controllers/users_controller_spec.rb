require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  fixtures :users

  before do
    @user = users(:calvin)
    @other_user = users(:florian)
  end
  #let(:user) { @user = users(:calvin) }
  #let(:other_user) { @other_user = users(:florian) }

  it "get new" do
    get :new
    expect(response.status).to eq 200
  end

  it "redirect edit when not logged-in" do
    get :edit, id: @user
    expect(flash.empty?).to be false
    #flash.empty?.should be false
    expect(response).to redirect_to login_url
  end

  it "redirect update when not logged-in" do
    patch :update, id: @user , user: { name: @user.name, email: @user.email}
    expect(flash.empty?).to be false
    expect(response).to redirect_to login_url
  end

  it "redirect edit when logged in as a wrong user" do
    log_in_as(@other_user)
    get :edit, id: @user
    expect(flash.empty?).to be true
    expect(response).to redirect_to root_url
  end

  it "redirect update when logged in as a wrong user" do
    log_in_as @other_user
    get :update, id: @user
    expect(flash.empty?).to be true
    expect(response).to redirect_to root_url
  end

  it "redirect index when not logged in" do
    get :index
    expect(response).to redirect_to login_url
  end

  it "redirect destroy when not logged in" do
    delete :destroy, id: @user
    expect(response).to redirect_to login_url
  end

  it "redirect destroy when logged-in as a non admin" do
    log_in_as(@other_user)
    delete :destroy, id: @user
    expect(response).to redirect_to root_url
  end

  it "redirect following when not logged in" do
    get :following, id: @user
    expect(response).to redirect_to login_url
  end

  it "redirect followers when not logged in" do
    get :followers, id: @user
    expect(response).to redirect_to login_url
  end

end
