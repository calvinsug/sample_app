require 'rails_helper'

RSpec.describe Relationship, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  let(:relationship) { Relationship.new(follower_id: 1, followed_id: 2) }
  #before{
  #  @relationship = Relationship.new(follower_id: 1, followed_id: 2)
  #}

  context 'validation' do

    it "is valid" do
      #expect(@relationship).to be_valid
      expect(relationship).to be_valid
      #relationship.should be_valid
    end

    it "has a follower id" do
      #@relationship.followed_id = nil
      #expect(@relationship.save).to be false
      relationship.followed_id = nil
      expect(relationship.save).to be false
    end

    it "has a follower id" do
      #@relationship.follower_id = nil
      #expect(@relationship.save).to be false
      relationship.follower_id = nil
      expect(relationship.save).to be false
    end

  end



end
