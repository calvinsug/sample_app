require 'rails_helper'

RSpec.describe User, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  before{
    #@user = User.new
    @user = User.create!({ name: 'Calvin Sugianto', email: 'calvin@calvin.com', password: 'foobar', password_confirmation: 'foobar'})
  }
  #@calvin = User.new({ name: 'Calvin Sugianto', email: 'calvinsug@pivoteapp.com', password: 'foobar'})

  fixtures :users

  describe 'validation' do
    it "should be valid" do
      expect(@user).to be_valid
    end

    it "has name" do
      @user.name = ' '
      expect(@user.save).to be false
    end

    it "has email" do
      @user.email = ' '
      expect(@user.save).to be false
    end

    it "name should not exceed 50 characters" do
      @user.name = 'a' * 51
      expect(@user.save).to be false
    end

    it "email should not exceed 255 characters" do
      @user.email= "a" * 244 + "@example.com"
      expect(@user.save).to be false
    end

    it "has a valid email address" do
      valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
      valid_addresses.each do |valid_address|
        @user.email = valid_address
        expect(@user).to be_valid
        #expect(@user.save).to be true
        #assert @user.valid?, "#{valid_address.inspect} should be valid"
      end
    end

    it "has no invalid email address" do
      invalid_address = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
      invalid_address.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid
        #expect(@user.save).to be false
        #assert_not @user.valid? , "#{invalid_address.inspect} should be invalid"
      end
    end

    it "has an unique email address" do
      duplicate_user = @user.dup
      duplicate_user.email = @user.email.upcase
      @user.save
      expect(duplicate_user).not_to be_valid
    end

    it "has a lowercase email address" do
      mixed_case_email = "Foo@ExaMpLe.com"
      @user.email = mixed_case_email
      @user.save
      expect(mixed_case_email.downcase).to eq(@user.reload.email)
      #assert_equal mixed_case_email.downcase, @user.reload.email
    end

    it "has password" do
      @user.password = ' '
      expect(@user).not_to be_valid
      #expect(@user.save).to be false
    end

    it "has a min 6 characters password" do
      @user.password = @user.password_confirmation = 'a' * 6
      expect(@user).to be_valid
    end

  end

  describe "behavior" do
    it "has false authentication with nil digest" do
      #expect(@user.authenticated?(:remember, '')).to be false
      @user.authenticated?(:remember,'').should be_falsey
    end

    it "has associated micropost" do
      @user.save
      @user.microposts.create!(content: "Lorem ipsum")
      @user.destroy
    end

    it "should follow and unfollow user" do
      calvin = users(:calvin)
      florian = users(:florian)
      calvin.following?(florian).should be false
      calvin.follow(florian)
      calvin.following?(florian)
      florian.followers.include?(calvin).should be true
      calvin.unfollow(florian)
      calvin.following?(florian).should be false
    end

    it "feed should have the right posts" do
      calvin = users(:calvin)
      florian = users(:florian)
      briant = users(:briant)

      # Posts from followed user
      calvin.microposts.each do |post_following|
        florian.feed.include?(post_following).should be true
      end

      # Posts from self
      calvin.microposts.each do |post_self|
        calvin.feed.include?(post_self).should be true
      end

      # Posts from unollowed user
      briant.microposts.each do |post_following|
        expect(florian.feed.include?(post_following)).to be false
      end
    end

  end


end