require 'rails_helper'

RSpec.describe Micropost, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"
  subject(:micropost) { Micropost.new({ content: 'testing tweet', user_id: 1}) }

  describe 'validation' do
    #before {
    #  @micropost = Micropost.new({ content: 'testing tweet', user_id: 1 })
    #}

    it 'is valid' do
     expect(micropost).to be_valid
    end

    it 'should not exceed 140 characters' do
      micropost.content = "a" * 141
      expect(micropost.save).to be(false)
    end

    it 'has content' do
      micropost.content = ' '
      expect(micropost.save).to be false
    end

    it 'has user id' do
      micropost.user_id = nil
      expect(micropost.save).to be false
    end

  end

  #it 'ordered by most recent first' do
    #Micropost.first.content.should == 'I`m learning Ruby on Rails'
  #  expect(Micropost.first.content).to eq("I`m learning Ruby on Rails")
  #end

end
