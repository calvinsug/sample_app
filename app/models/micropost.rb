class Micropost < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140}
  before_create  :picture_size

  def save
    puts 'before save'
    result = super
    puts 'after save'
    result
  end

  private

  # Validates the size of an uploaded picture.
  def picture_size
    puts "picture size: #{picture.size}"
    if picture.size > 5.megabytes
      errors.add(:picture, "should be less than 5MB")
    end
  end

end
